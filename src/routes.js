import Vue from 'vue'
import VueRouter from 'vue-router'

import Home from './Home.vue'
import Adherent from './components/Adherent.vue'

Vue.use(VueRouter);

const routes = [
    {
        path: '/',
        name: 'home',
        component: Home
    },
    {
        path: '/adherent',
        name: 'adherent',
        component: Adherent
    }
    
];

const router = new VueRouter({
    routes
});

export default router;